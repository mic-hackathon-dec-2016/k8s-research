- Doc: http://kubernetes.io/docs/user-guide/cron-jobs/
- kube-up by default doesn't configure the API server to support
batch/v2alpha1. Had to kube-up again with an env variable exported
$ export KUBE_RUNTIME_CONFIG="batch/v2alpha1=true"
Cron Jobs are only supported >= K8S API Server 1.5. Our K8S API server
is 1.4


kubectl apply -f scheduledjob.yaml
kubectl get scheduledjob 
kubectl describe scheduledjob hello 
kubectl get job --watch
kubectl get scheduledjob hello
kubectl get pods --selector=job-name=hello-1481698320
kubectl get pods --selector=job-name=hello-1481698320 --show-all
kubectl logs hello-1481698320-rvqnw 
kubectl delete scheduledjob hello 
